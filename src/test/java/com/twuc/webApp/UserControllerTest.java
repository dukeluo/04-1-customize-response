package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static java.util.jar.Attributes.Name.CONTENT_TYPE;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    // 2.1
    @Test
    void should_return_void_when_the_api_has_no_response() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/no-return-value"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    // 2.2
    @Test
    void should_return_void_and_status_code_when_the_api_has_no_renponse() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/no-return-value-with-annotation"))
                .andExpect(MockMvcResultMatchers.status().is(204));
    }

    // 2.3
    @Test
    void should_return_the_message_passed() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/messages/hello"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
                .andExpect(MockMvcResultMatchers.content().string("hello"));
    }

    // 2.4
    @Test
    void should_return_a_object() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/message-objects/hello"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType("application/json;charset=UTF-8"))
//                .andExpect(MockMvcResultMatchers.content().string("{\"message\":\"hello\"}"))
                .andExpect(jsonPath("$.message").value("hello"));
    }

    // 2.5
    @Test
    void should_return_a_object_with_202() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/message-objects-with-annotation/hello"))
                .andExpect(MockMvcResultMatchers.status().is(202))
                .andExpect(MockMvcResultMatchers.content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.message").value("hello"));
    }

    // 2.6
    @Test
    void should_return_a_object_with_header() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/message-entities/hello"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.header().string("X-Auth", "me"))
                .andExpect(MockMvcResultMatchers.content().contentType("application/json;charset=UTF-8"))
                .andExpect(MockMvcResultMatchers.header().string("content-type", "application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.message").value("hello"));
    }
}