package com.twuc.webApp;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    // 2.1
    @GetMapping("/api/no-return-value")
    public void getNoReturnValue() {
    }

    // 2.2
    @GetMapping("/api/no-return-value-with-annotation")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void getNoReturnValueWithStatusCode() {
    }

    // 2.3
    @GetMapping("/api/messages/{message}")
    public String getMessage(@PathVariable("message") String message) {
        return message;
    }

    // 2.4
    @GetMapping("/api/message-objects/{message}")
    public Message getObject(@PathVariable("message") String message) {
        return new Message(message);
    }

    // 2.5
    @GetMapping("/api/message-objects-with-annotation/{message}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Message getObjectWithStatus(@PathVariable("message") String message) {
        return new Message(message);
    }

    // 2.6
    @GetMapping("/api/message-entities/{message}")
    public ResponseEntity<Message> getObjectWithHeader(@PathVariable("message") String message) {
        HttpHeaders headers = new HttpHeaders();

        headers.add("X-Auth", "me");
        return new ResponseEntity<Message>(
                new Message(message),
                headers,
                HttpStatus.OK);
    }
}
